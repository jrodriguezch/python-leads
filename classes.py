from elasticsearch import Elasticsearch, RequestsHttpConnection
from db import DB
# from requests_aws4auth import AWS4Auth
import logging
import json
import multiprocessing
import time
import math
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Elastic:
    def __init__(self, fechaDesde, fechaHasta):
        # self.conection = self.getConecction()
        self.indexs = [
            # "st_visitaresultados",
            # "st_visitadetalle",
            "st_cliccontacto",
            "st_clicvertelefono",
            "st_clictocall",
            "st_clicwhatsapp"
        ]
        self.fechaDesde = fechaDesde
        self.fechaHasta = fechaHasta
        self.idsConsultados = []
        self.threads = []
        self.totalData = []
        self.paginasList = []
        self.inmosList = []
        self.dataReal = []
        self.db = DB().connection()

    def getConecction(self):
        return Elasticsearch(
            ['199cca30659545448f23a95cd04ff6d0.us-east-1.aws.found.io'],
            http_auth=('applicationel', 'Soluciones*2019$'),
            scheme="https",
            port=9243,
            ca_certs=False,
            verify_certs=False
            # maxsize=1000000
        )

    def getConecctionESI(self):
        return Elasticsearch(
            ['75da33c0ada64bb5ab76e3f12f8c5af5.us-east-1.aws.found.io'],
            http_auth=('www-ciencuadras-app', 'wrBr8hhmfhUSrAwWL'),
            scheme="https",
            port=9243,
            ca_certs=False,
            verify_certs=False
        )

    def getIdsInmos(self):
        cur = self.db.cursor()
        cur.execute("SELECT u.id FROM c1cc_new.usuario u where u.activo = '0' and id_tipo_usuario in ('2','10') AND (select count(i.id) from c1cc_new.inmueble i where i.id_usuario = u.id and i.activo IN ('0','1')) > 0;")
        for row in cur.fetchall():
            self.inmosList.append(row[0])
        print("Termino de Traer los IDS de la BD")
        self.db.close()

    def getInfoInms(self,codigos):
        cur = self.db.cursor()
        cur.execute("""select
                            i.id,
                            i.codigo,
                            i.activo,
                            u.id_tipo_usuario as tipo_usuario,
                            "INMUEBLE" as tipo
                        from
                            c1cc_new.inmueble i
                        inner JOIN c1cc_new.usuario u on
                            u.id = i.id_usuario
                        where
                            i.codigo in ("""+codigos+""")
                        union select
                            p.id_proyecto as id,
                            p.codigo_proyecto as codigo,
                            p.estado as activo,
                            4 as tipo_usuario,
                            "PROYECTO" as tipo
                        from
                            c1cc_new.proyectos p
                        where
                            p.codigo_proyecto in ("""+codigos+""")
                        union select
                            it.id_inmueble_tipo as id,
                            it.codigo_tipo_propiedad as codigo,
                            it.estado as activo,
                            4 as tipo_usuario,
                            "INMUEBLE TIPO" as tipo
                        from
                            c1cc_new.inmuebles_tipo it
                        where
                            it.codigo_tipo_propiedad in ("""+codigos+""")""")
        # f = open("consulta.txt", "a")
        # f.write("""select
        #                     i.id,
        #                     i.codigo,
        #                     i.activo,
        #                     u.id_tipo_usuario as tipo_usuario,
        #                     "INMUEBLE" as tipo
        #                 from
        #                     c1cc_new.inmueble i
        #                 inner JOIN c1cc_new.usuario u on
        #                     u.id = i.id_usuario
        #                 where
        #                     i.codigo in ("""+codigos+""")
        #                 union select
        #                     p.id_proyecto as id,
        #                     p.codigo_proyecto as codigo,
        #                     p.estado as activo,
        #                     4 as tipo_usuario,
        #                     "PROYECTO" as tipo
        #                 from
        #                     c1cc_new.proyectos p
        #                 where
        #                     p.codigo_proyecto in ("""+codigos+""")
        #                 union select
        #                     it.id_inmueble_tipo as id,
        #                     it.codigo_tipo_propiedad as codigo,
        #                     it.estado as activo,
        #                     4 as tipo_usuario,
        #                     "INMUEBLE TIPO" as tipo
        #                 from
        #                     c1cc_new.inmuebles_tipo it
        #                 where
        #                     it.codigo_tipo_propiedad in ("""+codigos+""")""")
        # f.close()
        dataJson = {}
        for row in cur.fetchall():
            dataJson.update({
                str(row[1]).upper(): {
                    "id": row[0],
                    "activo": row[2],
                    "tipo_usuario": row[3],
                    "tipo": row[4],
                }
            })
        print("Termino todo la info")
        self.db.close()
        return dataJson;

    def getIdsPaginas(self):
        cur = self.db.cursor()
        cur.execute("SELECT i.id FROM c1cc_new.inmueble i inner join c1cc_new.usuario u on (u.id = i.id_usuario and u.id_tipo_usuario in (2,10)) WHERE i.activo IN ('0','1') and u.activo = '0' order by id asc")
        for row in cur.fetchall():
            self.paginasList.append(str(row[0]))
        print("Termino de Traer los IDS de la BD")
        self.db.close()
        # es = self.getConecctionESI()
        # body = {
        #     "_source": {
        #         "excludes": [
        #             "@version",
        #             "@timestamp",
        #             "type",
        #             "*"
        #         ]
        #     },
        #     "query": {
        #         "bool": {
        #             "must": [
        #                 {
        #                     "bool": {
        #                         "must": [
        #                             {
        #                                 "terms": {
        #                                     "activo": [
        #                                         0,1
        #                                     ]
        #                                 }
        #                             }
        #                         ]
        #                     }
        #                 }
        #             ]
        #         }
        #     },
        #     "sort": [
        #         {
        #             "id": {
        #                 "order": "asc"
        #             }
        #         }
        #     ]
        # }

        # logging.info("Va empezar con los primeros hits")

        # result = es.search(
        #     index="results",
        #     body=body,
        #     size=10000,
        #     scroll="1m",
        #     request_timeout=12000)

        # logging.info("Termino con los primeros hits")
        # # print(json.dumps(result, indent=4, sort_keys=True))

        # print("Total de hits: " + str(len(result["hits"]["hits"])))

        # for hit in result["hits"]["hits"]:
        #     self.paginasList.append(hit['_id'])

        # scroll_id = result['_scroll_id']
        # scroll_size = result["hits"]["total"]["value"]
        # print("Total de hits a recorrer: {0}".format(scroll_size))

        # print("Total de veces a recorrer: " +
        #       str(math.ceil(scroll_size / 10000)))
        # i = 0
        # while(scroll_size > 0):
        #     logging.warning("Scrolling ({})...".format(i))
        #     result = es.scroll(scroll_id=scroll_id,
        #                        scroll="1m", request_timeout=12000)
        #     scroll_id = result["_scroll_id"]
        #     scroll_size = len(result['hits']['hits'])
        #     for hit in result["hits"]["hits"]:
        #         self.paginasList.append(hit['_id'])

        #     logging.warning("Termino Scrolling ({})...".format(i))
        #     i = i + 1

    def getDataConsolidado(self):
        es = self.getConecction()
        body = {
            "_source": {
                "excludes": [
                    "@version",
                    "@timestamp",
                    "type"
                ]
            },
            "query": {
                "bool": {
                    "must": [
                        {
                            "bool": {
                                "must": {
                                    "range": {
                                        "fecha": {
                                            "gte": self.fechaDesde,
                                            "lte": self.fechaHasta
                                        }
                                    }
                                }
                            }
                        },
                        # {
                        #     "bool": {
                        #         "must": {
                        #             "terms": {
                        #                 "idUsuario": self.inmosList
                        #             }
                        #         }
                        #     }
                        # },
                    ],
                    "filter": []
                }
            },
            "sort": [],
            "size": 0,
            "aggs": {
                "byindex": {
                    "terms": {
                        "field": "_index"
                    },
                    "aggs": {
                        "total": {
                            "sum": {
                                "field": "cantidad"
                            }
                        }
                    }
                }
            }
        }
        res = es.search(
            request_timeout=120, index=self.indexs, body=body)
        dataArray = {
            "fecha": self.fechaDesde + " al " +  self.fechaHasta
        }
        dataInfo = []
        for index in res['aggregations']['byindex']['buckets']:
            dataArray.update(
                {index['key']: int(float(index['total']['value']))})
            
        dataInfo.append(dataArray)
        return dataInfo;
        # print(json.dumps(res, indent=4, sort_keys=True))
        # return self.createArrayResult(res, "idInmueble")
    
    def getDataInmueble(self, ids=[], length=300, es=""):
        body = {
            "_source": {
                "excludes": [
                    "@version",
                    "@timestamp",
                    "type"
                ]
            },
            "query": {
                "bool": {
                    "must": [
                        # {
                        #     "bool": {
                        #         "must": {
                        #             "terms": {
                        #                 "idInmueble": ids
                        #             }
                        #         }
                        #     }
                        # },
                        {
                            "bool": {
                                "must": {
                                    "range": {
                                        "fecha": {
                                            "gte": self.fechaDesde,
                                            "lte": self.fechaHasta
                                        }
                                    }
                                }
                            }
                        }
                    ],
                    "filter": []
                }
            },
            "sort": [],
            "size": 0,
            "aggs": {
                "data": {
                    "terms": {
                        "field": "codigo",
                        "size": 500000,
                        "order": {
                            "_key": "asc"
                        }
                    },
                    "aggs": {
                        # "id_usuario": {
                        #     "terms": {
                        #         "field": "idUsuario",
                        #         "size": 1
                        #     }
                        # },
                        # "nombre_usuario": {
                        #     "terms": {
                        #         "field": "nombre_usuario",
                        #         "size": 1
                        #     }
                        # },
                        # "codigo": {
                        #     "terms": {
                        #         "field": "codigo",
                        #         "size": 1
                        #     }
                        # },
                        "byindex": {
                            "terms": {
                                "field": "_index"
                            },
                            "aggs": {
                                "total": {
                                    "sum": {
                                        "field": "cantidad"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        res = es.search(
            request_timeout=120, index=self.indexs, body=body)
        return self.createArrayResult(res, "idInmueble")

    def createArrayResult(self, data, group):
        dataInfo = []
        for bucket in data['aggregations']['data']['buckets']:
            if group == "idUsuario":
                dataArray = {
                    "id_usuario": bucket['key'],
                    # "nombre_usuario": bucket['usuario']['buckets'][0]['key']
                }
                for index in bucket['byindex']['buckets']:
                    dataArray.update(
                        {index['key']: int(float(index['total']['value']))})

                for index2 in self.indexs:
                    if index2 not in dataArray:
                        dataArray.update({index2: 0})

                dataInfo.append(dataArray)

            if group == "idInmueble":
                dataArray = {
                    "codigo": bucket['key'],
                    # "id_usuario": bucket['id_usuario']['buckets'][0]['key'],
                    # "nombre_usuario": bucket['nombre_usuario']['buckets'][0]['key'],
                    # "codigo": bucket['codigo']['buckets'][0]['key'] if(len(bucket['codigo']['buckets']) > 0) else ''
                }
                for index in bucket['byindex']['buckets']:
                    dataArray.update(
                        {index['key']: int(float(index['total']['value']))})

                for index2 in self.indexs:
                    if index2 not in dataArray:
                        dataArray.update({index2: 0})

                dataInfo.append(dataArray)

        return dataInfo

    def functRecursive(self, inms, group="idInmueble", es="",L=[]):
        try:
            # logging.error("Va empezar el hilo")
            # logging.info("Va empezar a hilo con: desde: " +
            #              str(desde) + " hasta: " + str(hasta) + " countRows: " + str(countRows))
            # pag = self.getPaginations(desde, hasta, group, countRows, es)
            # ids = pag['ids']
            # if(len(ids) > 0):
            data = self.getDataInmueble(inms, len(inms), es)
            # data = self.getDataUsuarios(ids, hasta)
            for item in data:
                L.append(item)
            # else:
            #     logging.warning(
            #         'Ya no hay mas leads que buscar en la pagina: ' + str(desde) + " hasta: " + str(hasta))
            # logging.error("Termino el hilo")
            # logging.info("Termino el hilo con: desde: " +
            #              str(desde) + " hasta: " + str(hasta))
        except:
            logging.error("Error en el thread")
            logging.error(inms)
            # logging.error("Error en la pagina: " +
            #               str(desde) + " hasta: " + str(hasta))
            # time.sleep(1)
            # x = self.createThread(desde,hasta,group,countRows)
            # x.join()
            # x = threading.Thread(target=functRecursive, args=(desde,hasta,"idInmueble"))
            # x.start()
            # x.join()

    def createThread(self, inms, group, es,L):
        x = multiprocessing.Process(target=self.functRecursive,
                                    args=(inms, group, es,L))
        self.threads.append(x)
        # x.start()
        # return x