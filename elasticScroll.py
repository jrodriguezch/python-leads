from elasticsearch import Elasticsearch
import json
import math
import logging

format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

es = Elasticsearch(
    ['199cca30659545448f23a95cd04ff6d0.us-east-1.aws.found.io'],
    http_auth=('applicationel', 'Soluciones*2019$'),
    scheme="https",
    port=9243,
    maxsize=1000000,
    size=2000,
    scroll="1m"
)
indexs = [
    "st_visitaresultados",
    "st_visitadetalle",
    "st_cliccontacto",
    "st_clicvertelefono",
    "st_clictocall",
    "st_clicwhatsapp"
]
body = {
    "_source": {
        "excludes": [
            "@version",
            "@timestamp",
            "type"
        ]
    },
    "query": {
        "bool": {
            "must": [
                {
                    "bool": {
                        "must": {
                            "range": {
                                "fecha": {
                                    "gte": "2019-09-01",
                                    "lte": "2019-09-30"
                                }
                            }
                        }
                    }
                }
            ],
            "filter": []
        }
    },
    "sort": [
        {
            "idInmueble": {
                "order": "asc"
            }
        }, {
            "_index": {
                "order": "desc"
            }
        }
    ]
}
logging.info("Va empezar con los primeros hits")
result = es.search(
    index=indexs,
    body=body,
    size=1000,
    scroll="1m",
    request_timeout=300)
logging.info("Termino con los primeros hits")

data = []
for hit in result["hits"]["hits"]:
    # for d in hit["_source"]["attributes"]["data_of_interest"]:
    data.append(hit['_source'])
        # do_something(*args)

scroll_id = result['_scroll_id']
scroll_size = result["hits"]["total"]["value"]

print("Total de hits a recorrer: {0}".format(scroll_size))

print("Total de veces a recorrer: " +  str(math.ceil(scroll_size / 1000)))

i = 0

while(scroll_size > 0):
    logging.warning("Scrolling ({})...".format(i))
    # print("Scrolling ({})...".format(i))
    result = es.scroll(scroll_id=scroll_id, scroll="1m", request_timeout=300)
    scroll_id = result["_scroll_id"]
    scroll_size = len(result['hits']['hits'])
    for hit in result["hits"]["hits"]:
        data.append(hit['_source'])

    logging.warning("Termino Scrolling ({})...".format(i))
    i = i + 1

print(json.dumps(data, indent=4, sort_keys=True))

