from elasticsearch import Elasticsearch
import logging
import multiprocessing
import time
# logging.basicConfig(format=format, level=logging.WARNING,
#                     datefmt="%H:%M:%S")


class Elastic:
    def __init__(self, fechaDesde, fechaHasta):
        # self.conection = self.getConecction()
        self.indexs = [
            "st_visitaresultados",
            # "st_visitadetalle",
            # "st_cliccontacto",
            # "st_clicvertelefono",
            # "st_clictocall",
            # "st_clicwhatsapp"
        ]
        self.fechaDesde = fechaDesde
        self.fechaHasta = fechaHasta
        self.idsConsultados = []
        self.threads = list()
        self.totalData = []

    def getConecction(self):
        return Elasticsearch(
            ['199cca30659545448f23a95cd04ff6d0.us-east-1.aws.found.io'],
            http_auth=('applicationel', 'Soluciones*2019$'),
            scheme="https",
            port=9243,
            # maxsize=1000000
        )

    def getDataUsuarios(self, ids=[], length=300, es=""):
        body = {
            "_source": {
                "excludes": [
                    "@version",
                    "@timestamp",
                    "type"
                ]
            },
            "query": {
                "bool": {
                    "must": [
                        {
                            "bool": {
                                "must": {
                                    "terms": {
                                        "idUsuario": ids
                                    }
                                }
                            }
                        },
                        {
                            "bool": {
                                "must": {
                                    "range": {
                                        "fecha": {
                                            "gte": self.fechaDesde,
                                            "lte": self.fechaHasta
                                        }
                                    }
                                }
                            }
                        }
                    ],
                    "filter": []
                }
            },
            "sort": [],
            "size": 0,
            "aggs": {
                "data": {
                    "terms": {
                        "field": "idUsuario",
                        "size": length,
                        "order": {
                            "_key": "asc"
                        }
                    },
                    "aggs": {
                        # "usuario": {
                        #     "terms": {
                        #         "field": "nombre_usuario",
                        #         "size": 1
                        #     }
                        # },
                        "byindex": {
                            "terms": {
                                "field": "_index"
                            },
                            "aggs": {
                                "total": {
                                    "sum": {
                                        "field": "cantidad"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        res = es.search(
            request_timeout=120, index=self.indexs, body=body)
        return self.createArrayResult(res, "idUsuario")

    def getDataInmueble(self, ids=[], length=300, es=""):
        body = {
            "_source": {
                "excludes": [
                    "@version",
                    "@timestamp",
                    "type"
                ]
            },
            "query": {
                "bool": {
                    "must": [
                        {
                            "bool": {
                                "must": {
                                    "terms": {
                                        "idInmueble": ids
                                    }
                                }
                            }
                        },
                        {
                            "bool": {
                                "must": {
                                    "range": {
                                        "fecha": {
                                            "gte": self.fechaDesde,
                                            "lte": self.fechaHasta
                                        }
                                    }
                                }
                            }
                        }
                    ],
                    "filter": []
                }
            },
            "sort": [],
            "size": 0,
            "aggs": {
                "data": {
                    "terms": {
                        "field": "idInmueble",
                        "size": length,
                        "order": {
                            "_key": "asc"
                        }
                    },
                    "aggs": {
                        # "id_usuario": {
                        #     "terms": {
                        #         "field": "idUsuario",
                        #         "size": 1
                        #     }
                        # },
                        # "nombre_usuario": {
                        #     "terms": {
                        #         "field": "nombre_usuario",
                        #         "size": 1
                        #     }
                        # },
                        # "codigo": {
                        #     "terms": {
                        #         "field": "codigo",
                        #         "size": 1
                        #     }
                        # },
                        "byindex": {
                            "terms": {
                                "field": "_index"
                            },
                            "aggs": {
                                "total": {
                                    "sum": {
                                        "field": "cantidad"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        res = es.search(
            request_timeout=120, index=self.indexs, body=body)
        return self.createArrayResult(res, "idInmueble")

    def createArrayResult(self, data, group):
        dataInfo = []
        for bucket in data['aggregations']['data']['buckets']:
            if group == "idUsuario":
                dataArray = {
                    "id_usuario": bucket['key'],
                    # "nombre_usuario": bucket['usuario']['buckets'][0]['key']
                }
                for index in bucket['byindex']['buckets']:
                    dataArray.update(
                        {index['key']: int(float(index['total']['value']))})

                for index2 in self.indexs:
                    if index2 not in dataArray:
                        dataArray.update({index2: 0})

                dataInfo.append(dataArray)

            if group == "idInmueble":
                dataArray = {
                    "id_inmueble": bucket['key'],
                    # "id_usuario": bucket['id_usuario']['buckets'][0]['key'],
                    # "nombre_usuario": bucket['nombre_usuario']['buckets'][0]['key'],
                    # "codigo": bucket['codigo']['buckets'][0]['key'] if(len(bucket['codigo']['buckets']) > 0) else ''
                }
                for index in bucket['byindex']['buckets']:
                    dataArray.update(
                        {index['key']: int(float(index['total']['value']))})

                for index2 in self.indexs:
                    if index2 not in dataArray:
                        dataArray.update({index2: 0})

                dataInfo.append(dataArray)

        return dataInfo

    def functRecursive(self, desde=0, hasta=300, group="idInmueble", countRows=0, es=""):
        try:
            logging.info("Va empezar a hilo con: desde: " +
                         str(desde) + " hasta: " + str(hasta) + " countRows: " + str(countRows))
            pag = self.getPaginations(desde, hasta, group, countRows,es)
            ids = pag['ids']
            if(len(ids) > 0):
                data = self.getDataInmueble(ids, hasta, es)
                # data = self.getDataUsuarios(ids, hasta)
                for item in data:
                    self.totalData.append(item)
            else:
                logging.warning(
                    'Ya no hay mas leads que buscar en la pagina: ' + str(desde) + " hasta: " + str(hasta))

            logging.info("Termino el hilo con: desde: " +
                         str(desde) + " hasta: " + str(hasta))
        except:
            logging.error("Error en la pagina: " +
                          str(desde) + " hasta: " + str(hasta))
            # time.sleep(1)
            # x = self.createThread(desde,hasta,group,countRows)
            # x.join()
            # x = threading.Thread(target=functRecursive, args=(desde,hasta,"idInmueble"))
            # x.start()
            # x.join()

    def getPaginations(self, desde=0, hasta=1000, group="idInmueble", countRows=0, es=""):
        body = {
            "_source": {
                "excludes": [
                    "@version",
                    "@timestamp",
                    "type",
                    "*"
                ]
            },
            "query": {
                "bool": {
                    "must": [
                        {
                            "bool": {
                                "must": {
                                    "range": {
                                        "fecha": {
                                            "gte": self.fechaDesde,
                                            "lte": self.fechaHasta
                                        }
                                    }
                                }
                            }
                        }
                    ],
                    "filter": []
                }
            },
            "sort": [
                {
                    "idUsuario": {
                        "order": "asc"
                    }
                }
            ],
            "from": desde,
            "size": hasta,
            "collapse": {
                "field": group
            }
        }
        if(countRows <= 0):
            totalCount = {
                "aggs": {
                    "total_count": {
                        "cardinality": {
                            "field": group,
                            "precision_threshold": 40000
                        }
                    }
                }
            }
            body.update(totalCount)
        res = es.search(
            request_timeout=120, index=self.indexs, body=body)
        totalRows = res["aggregations"]["total_count"]["value"] if countRows <= 0 else countRows
        ids = []
        for item in res['hits']['hits']:
            if("fields" in item and item["fields"][group][0] not in self.idsConsultados):
                ids.append(item["fields"][group][0])
                self.idsConsultados.append(item["fields"][group][0])

        return {'totalRows': totalRows, 'ids': ids}

    def createThread(self, start, limit, group, countRows, es):
        x = multiprocessing.Process(target=self.functRecursive,
                                    args=(start, limit, group, countRows, es))
        self.threads.append(x)
        # x.start()
        # return x
