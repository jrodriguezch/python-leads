from classes import Elastic as Elastica
import json
import logging
import math
import pandas as pd
import time
from multiprocessing import Manager
import pickle


elastica = Elastica("2019-01-01", "2019-10-22")

leads = pd.DataFrame()

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.WARNING,
                        datefmt="%H:%M:%S")
    isValidAsw = False
    while(isValidAsw == False):
        try:
            isFIleInms = int(input("Desea usar el archivo guardado de inmuebles?\n1. Si\n2. No\n"))
            isValidAsw = True
        except:
            isValidAsw = False
            print("Parece que el valor ingresado no es correto, ingresa el numero 1 o 2")
    logging.warning("Va empezar todo")
    with Manager() as manager:
        L = manager.list()
        if(isFIleInms == 1):
            elastica.getIdsPaginas()
            # print("Total ids: " + str(len(elastica.paginasList)))
            with open("ids_inms.txt", "wb") as fp:
                pickle.dump(elastica.paginasList, fp)
        else:
            with open("ids_inms.txt", "rb") as fp:
                elastica.paginasList = pickle.load(fp)
            print("Total ids: " + str(len(elastica.paginasList)))

        limitForPag = 1000

        limitForReq = 100

        chunk = [elastica.paginasList[i : i + limitForPag] for i in range(0, len(elastica.paginasList), limitForPag)]

        chunk2 = [chunk[i : i + limitForReq] for i in range(0, len(chunk), limitForReq)]

        # print(json.dumps(chunk2, indent=4, sort_keys=True))
        print("Total de ciclos: " + str(len(chunk2)))
        i = 1
        for pag in chunk2:
            con = 0
            elastica.threads = []
            esList = [elastica.getConecction() for con in range(limitForPag)]

            logging.warning("Va crear todos los hilos")

            for inms in pag:
                # print(inms)
                es = esList[con]
                elastica.createThread(inms,"idInmueble",es,L)

            logging.warning("Va esperar que todos los hilos terminen")

            for x in elastica.threads:
                x.start()

            for x in elastica.threads:
                x.join()
            # print(len(L))
            logging.warning("Termino todos los hilos del ciclo: " + str(i))

            i = i + 1
        df=pd.DataFrame(list(L))
        leads=leads.append(df)
        leads.to_csv(r'prueba.csv',sep='\t')