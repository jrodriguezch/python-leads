from flask import Flask, send_file, request, render_template
from classes import Elastic as Elastica
import json
import logging
import math
import pandas as pd
import time
from multiprocessing import Manager
import pickle
import time

app = Flask(__name__,template_folder='templates')

fileName = ""


@app.route("/downloadReport",methods=['GET'])
def downloadReport():
    return send_file(request.args.get('fileName', None),
                     mimetype='text/csv',
                     attachment_filename=request.args.get('fileName', None),
                     as_attachment=True)

@app.route("/downloadReportInmue",methods=['GET'])
def downloadReportInms():
    return send_file(request.args.get('fileName', None),
                     mimetype='text/csv',
                     attachment_filename=request.args.get('fileName', None),
                     as_attachment=True)

@app.route("/generarReporteConsolidado", methods=['GET'])
def generarReporteConsolidado():
    dateIni = request.args.get('dateIni')
    dateFin = request.args.get('dateFin')
    isFileInms = request.args.get('isFileInms')
    elastica = Elastica(dateIni, dateFin)
    leads = pd.DataFrame()
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.WARNING,
                        datefmt="%H:%M:%S")
    logging.warning("Va empezar todo")
    with Manager() as manager:
        L = manager.list()
        data = elastica.getDataConsolidado()
        L = data;
        print(json.dumps(L, indent=4, sort_keys=True))
        df = pd.DataFrame(list(L))
        leads = leads.append(df)
        fileName = "Reporte_Consolidado"+str(int(time.time()))+".csv"
        leads.to_csv(fileName, sep='\t')
    return render_template('resultado.html',fileName=fileName)

@app.route("/generateReport", methods=['GET'])
def generateReport():
    dateIni = request.args.get('dateIni')
    dateFin = request.args.get('dateFin')
    elastica = Elastica(dateIni, dateFin)
    leads = pd.DataFrame()
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.WARNING,
                        datefmt="%H:%M:%S")
    logging.warning("Va empezar todo")
    inmuebles = elastica.getDataInmueble(es=elastica.getConecction())
    codigos = "'" + "','".join([inm.get('codigo') for inm in inmuebles]) + "'"
    dataInfo = elastica.getInfoInms(codigos=codigos)
    print(len(dataInfo))
    for idx, inm in enumerate(inmuebles):
        if(str(inm['codigo']).upper() in dataInfo):
            inmuebles[idx]['id'] = dataInfo[str(inm['codigo']).upper()]['id']
            inmuebles[idx]['activo'] = dataInfo[str(inm['codigo']).upper()]['activo']
            inmuebles[idx]['tipo_usuario'] = dataInfo[str(inm['codigo']).upper()]['tipo_usuario']
            inmuebles[idx]['tipo'] = dataInfo[str(inm['codigo']).upper()]['tipo']
        else:
            inmuebles[idx]['id'] = "N/A"
            inmuebles[idx]['activo'] = "N/A"
            inmuebles[idx]['tipo_usuario'] = "N/A"
            inmuebles[idx]['tipo'] = "N/A"
    df = pd.DataFrame(list(inmuebles))
    fileName = "Reporte_Inmuebles"+str(int(time.time()))+".csv"
    leads = leads.append(df)
    leads.to_csv(fileName, sep='\t')
    return render_template('resultado_Inms.html',fileName=fileName)

def prueba():
    dateIni = request.args.get('dateIni')
    dateFin = request.args.get('dateFin')
    isFileInms = request.args.get('isFileInms')
    limitPage = int(request.args.get('limitPage'))
    limitReq = int(request.args.get('limitReq'))
    # elastica = Elastica("2019-01-01", "2019-10-22")
    elastica = Elastica(dateIni, dateFin)
    leads = pd.DataFrame()
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.WARNING,
                        datefmt="%H:%M:%S")
    logging.warning("Va empezar todo")
    with Manager() as manager:
        L = manager.list()
        if(isFileInms == '1'):
            elastica.getIdsPaginas()
            # print("Total ids: " + str(len(elastica.paginasList)))
            with open("ids_inms.txt", "wb") as fp:
                pickle.dump(elastica.paginasList, fp)
        else:
            with open("ids_inms.txt", "rb") as fp:
                elastica.paginasList = pickle.load(fp)
            print("Total ids: " + str(len(elastica.paginasList)))

        limitForPag = limitPage

        limitForReq = limitReq

        chunk = [elastica.paginasList[i: i + limitForPag]
                 for i in range(0, len(elastica.paginasList), limitForPag)]

        chunk2 = [chunk[i: i + limitForReq]
                  for i in range(0, len(chunk), limitForReq)]

        # # print(json.dumps(chunk2, indent=4, sort_keys=True))
        print("Total de ciclos: " + str(len(chunk2)))
        i = 1
        for pag in chunk2:
            con = 0
            elastica.threads = []
            esList = [elastica.getConecction() for con in range(limitForPag)]

            logging.warning("Va crear todos los hilos")

            for inms in pag:
                # print(inms)
                es = esList[con]
                elastica.createThread(inms, "idInmueble", es, L)

            logging.warning("Va esperar que todos los hilos terminen")

            for x in elastica.threads:
                x.start()

            for x in elastica.threads:
                x.join()
            # print(len(L))
            logging.warning("Termino todos los hilos del ciclo: " + str(i))

            i = i + 1
        df = pd.DataFrame(list(L))
        fileName = "Reporte_Inmuebles"+str(int(time.time()))+".csv"
        leads = leads.append(df)
        leads.to_csv(fileName, sep='\t')
    return render_template('resultado_Inms.html',fileName=fileName)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000, threaded=True)
